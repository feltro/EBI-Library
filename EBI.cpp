/* EBI Interface

written by Luca Feltrin
*/

#include "EBI.h"

/*******************************************/
/*			EBI LoRaWAN Interface	 	   */
/*******************************************/

ExecutionStatus EBI_LoRaWAN::begin() {
	ExecutionStatus ESresult;
	this->_payloadPtr = &this->_tx_buffer[2];			//points to the beginning of the raw payload
	EBI_SERIAL.begin(9600);
	EBI_SERIAL.setTimeout(RX_TIMEOUT_MS);
	ExecutionStatus result = this->reset();
	if(result==EBIES_Success) {
		DEBUG_PRINTLN("Module Initialized");
		//**************************GET CURRENT VALUES********************//
		//Network Type
		this->_payloadPtr[0] = (uint8_t)EBICMD_NetworkType;			
		ESresult = this->performTxRx(1);
		if(ESresult!=EBIES_Success) return ESresult;
		networkType = (EBI_NetworkType)_rx_buffer[3];
		
		//Transmit Power
		this->_payloadPtr[0] = (uint8_t)EBICMD_OutputPower;
		ESresult = this->performTxRx(1);
		if(ESresult!=EBIES_Success) return ESresult;
		transmitPower = (int)this->_rx_buffer[3];
		
		//DevEUI / AppEUI
		this->_payloadPtr[0] = (uint8_t)EBICMD_PhysicalAddress;
		ESresult = this->performTxRx(1);
		if(ESresult!=EBIES_Success) return ESresult;		
		for(int i=0;i<8;i++) DevEUI[i] = this->_rx_buffer[3+i];
		for(int i=0;i<8;i++) AppEUI[i] = this->_rx_buffer[11+i];

		//DevAddress
		this->_payloadPtr[0] = (uint8_t)EBICMD_NetworkAddress;;
		ESresult = this->performTxRx(1);
		if(ESresult!=EBIES_Success) return ESresult;		
		for(int i=0;i<4;i++) DevAddress[i] = this->_rx_buffer[3+i];				
		
		//LoRaWAN Class
		this->_payloadPtr[0] = (uint8_t)EBICMD_EnergySave;		
		ESresult = this->performTxRx(1);
		if(ESresult!=EBIES_Success) return ESresult;
		lclass = (LoRaClass)_rx_buffer[3];
		
		//Join Mode, ADR + set LoRaWAN mode
		this->_payloadPtr[0] = (uint8_t)EBICMD_NetworkPreferences;		
		ESresult = performTxRx(1);
		if(ESresult!=EBIES_Success) return ESresult;
		joinMode = ((_rx_buffer[3]&0x40)==0)?ABP:OTAA;
		_ADR = (_rx_buffer[3]&0x20)!=0;
		
		//*********************Configure LoraWAN mode*********************
		int replyLength;
		this->_payloadPtr[0] = (uint8_t)EBICMD_NetworkPreferences;
		this->_payloadPtr[1] = 0x80;		//Set LoRaWAN protocol
		if(joinMode==OTAA) this->_payloadPtr[1] |= 0x40;
		if(_ADR) this->_payloadPtr[1] |= 0x20;		
		ESresult = this->performTxRx(2);
		if(ESresult!=EBIES_Success) return ESresult;
		ESresult = (ExecutionStatus)this->_rx_buffer[3];
		if(ESresult!=EBIES_Success) return ESresult;
		
		
	} else {
		DEBUG_PRINTLN("Failed to Reset");
	}
	return result;
}

int EBI_LoRaWAN::getUplinkMacCounter(){
	this->_payloadPtr[0] = (uint8_t)EBICMD_MacCounters;
	
	ExecutionStatus ESresult = this->performTxRx(1);
	
	if(ESresult!=EBIES_Success) return -1;
	
	return (this->_rx_buffer[3]<<24) | (this->_rx_buffer[4]<<16) | (this->_rx_buffer[5]<<8) | (this->_rx_buffer[6]<<0);
}

int EBI_LoRaWAN::getDownlinkMacCounter(){
	this->_payloadPtr[0] = (uint8_t)EBICMD_MacCounters;
	
	ExecutionStatus ESresult = this->performTxRx(1);
	
	if(ESresult!=EBIES_Success) return -1;
	
	return (this->_rx_buffer[7]<<24) | (this->_rx_buffer[8]<<16) | (this->_rx_buffer[9]<<8) | (this->_rx_buffer[10]<<0);
}

ExecutionStatus EBI_LoRaWAN::sendBytes(uint8_t data[], int length, int FPort){
	if(FPort<1 || FPort>223) return EBIES_ParametersNotAccepted;
	
	int payloadLength = length + 4 + (_ACK?1:0);
	this->_payloadPtr[0] = (uint8_t)EBICMD_SendData;
	
	this->_payloadPtr[1] = 0x08;
	if(_ACK) this->_payloadPtr[1] |= 0x06;	//Enable ACK and retries field
	this->_payloadPtr[1] |= 0x01;		//Uncomment for information after the transmission
	
	this->_payloadPtr[2] = 0x00;
	
	this->_payloadPtr[3] = (uint8_t)FPort;
	
	if(_ACK) this->_payloadPtr[4] = _retransmissions;
	
	for(int i=0;i<length;i++) this->_payloadPtr[4+(_ACK?1:0)+i] = data[i];	

	ExecutionStatus ESresult = this->performTxRx(payloadLength);
	if(ESresult!=EBIES_Success) return ESresult;

	_lastTxRetries = this->_rx_buffer[4];
	
	if(this->_rx_buffer[5]&0x80!=0) _lastTxAckRSSI = 0xFFFF0000;
	else _lastTxAckRSSI = 0;	
	_lastTxAckRSSI |= this->_rx_buffer[5]<<8 | this->_rx_buffer[6];
	if((ExecutionStatus)this->_rx_buffer[3]!=EBIES_Success) _lastTxAckRSSI = 0;
	
	_lastTxChannel = this->_rx_buffer[7]<<8 | this->_rx_buffer[8];
	
	_lastTxDR = this->_rx_buffer[9];
	
	_lastTxPtx = this->_rx_buffer[10];
	
	_lastTxWaitingTime = this->_rx_buffer[11]<<24 | this->_rx_buffer[12]<<16 | this->_rx_buffer[13]<<8 | this->_rx_buffer[14];
	
	//Wait for a DL message (only if Class A LoRa devices)
	if(lclass==ClassA) {
		EBI_SERIAL.setTimeout(DL_TIMEOUT_MS);
		int dlReplyLength = -1;
		ExecutionStatus dles = parseRxRaw(&dlReplyLength);
		if(dles==EBIES_Success){
			_DlDataAvailable=dlReplyLength-9;
			DEBUG_BUFFER_PRINTLN(_rx_buffer,dlReplyLength);
			
			//Parsing
			if(this->_rx_buffer[5]&0x80!=0) _lastRxRSSI = 0xFFFF0000;
			else _lastRxRSSI = 0;	
			_lastRxRSSI |= this->_rx_buffer[5]<<8 | this->_rx_buffer[6];
			
			_lastRxFPort = (int)this->_rx_buffer[7];

			for(int i=0;i<_DlDataAvailable;i++) _dl_buffer[i]=_rx_buffer[i+8];
			DEBUG_BUFFER_PRINTLN(_dl_buffer,_DlDataAvailable);
			
		}
		EBI_SERIAL.setTimeout(RX_TIMEOUT_MS);
	}
	
	return (ExecutionStatus)this->_rx_buffer[3];
}

int EBI_LoRaWAN::available(){
	return _DlDataAvailable;
}

int EBI_LoRaWAN::available(int timeout){
	if(lclass != ClassC) return available();
	
	EBI_SERIAL.setTimeout(timeout);
		int dlReplyLength = -1;
		ExecutionStatus dles = parseRxRaw(&dlReplyLength);
		if(dles==EBIES_Success){
			_DlDataAvailable=dlReplyLength-9;
			DEBUG_BUFFER_PRINTLN(_rx_buffer,dlReplyLength);
			
			//Parsing
			if(this->_rx_buffer[5]&0x80!=0) _lastRxRSSI = 0xFFFF0000;
			else _lastRxRSSI = 0;	
			_lastRxRSSI |= this->_rx_buffer[5]<<8 | this->_rx_buffer[6];
			
			_lastRxFPort = (int)this->_rx_buffer[7];

			for(int i=0;i<_DlDataAvailable;i++) _dl_buffer[i]=_rx_buffer[i+8];
			DEBUG_BUFFER_PRINTLN(_dl_buffer,_DlDataAvailable);
			
		}
		EBI_SERIAL.setTimeout(RX_TIMEOUT_MS);
	
	return _DlDataAvailable;
}

int EBI_LoRaWAN::readBytes(uint8_t buffer[], int length){
	int returnval = _DlDataAvailable;
	if(length<_DlDataAvailable) returnval = length;
	_DlDataAvailable = -1;	//clear the dl_buffer
	
	for(int i=0;i<returnval;i++) buffer[i] = _dl_buffer[i];	
	
	return returnval;
}

ExecutionStatus EBI_LoRaWAN::sendInt(int value, int FPort){
	this->sendBytes((uint8_t *)&value,sizeof(value),FPort);
}

ExecutionStatus EBI_LoRaWAN::sendFloat(float value, int FPort){
	this->sendBytes((uint8_t *)&value,sizeof(value),FPort);
}

ExecutionStatus EBI_LoRaWAN::disableDutyCycle(){
	_disableDutyCycle = true;
}

ExecutionStatus EBI_LoRaWAN::enableADR(){
	_ADR = true;
}

ExecutionStatus EBI_LoRaWAN::disableADR(){
	_ADR = false;
}

void EBI_LoRaWAN::enableAck(){
	this->_ACK = true;
}

void EBI_LoRaWAN::disableAck(){
	this->_ACK = false;
}

ExecutionStatus EBI_LoRaWAN::connect(){
	ExecutionStatus ESresult;
	int payloadLength;
	int replyLength;	
	
	//********************Join Mode, ADR*************************
	this->_payloadPtr[0] = (uint8_t)EBICMD_NetworkPreferences;
	this->_payloadPtr[1] = 0x80;
	if(joinMode==OTAA) this->_payloadPtr[1] |= 0x40;
	if(_ADR) this->_payloadPtr[1] |= 0x20;	
	ESresult = this->performTxRx(2);
	if(ESresult!=EBIES_Success) return ESresult;
	ESresult = (ExecutionStatus)this->_rx_buffer[3];
	if(ESresult!=EBIES_Success) return ESresult;
	
	//********************Disable Duty Cycle (if asked)*************************
	if(_disableDutyCycle){
		this->_payloadPtr[0] = 0x2C;
		this->_payloadPtr[1] = 0x00;		
		ESresult = this->performTxRx(2);
		if(ESresult!=EBIES_Success) return ESresult;
		ESresult = (ExecutionStatus)this->_rx_buffer[3];
		if(ESresult!=EBIES_Success) return ESresult;
	}
	
	//********************Transmit Power*************************
	this->_payloadPtr[0] = (uint8_t)EBICMD_OutputPower;
	this->_payloadPtr[1] = (uint8_t)transmitPower;	
	ESresult = this->performTxRx(2);
	if(ESresult!=EBIES_Success) return ESresult;
	transmitPower = (int)this->_rx_buffer[3];
	
	//********************Network Type*************************
	this->_payloadPtr[0] = (uint8_t)EBICMD_NetworkType;
	this->_payloadPtr[1] = (uint8_t)networkType;
	ESresult = this->performTxRx(2);
	if(ESresult!=EBIES_Success) return ESresult;
	ESresult = (ExecutionStatus)this->_rx_buffer[3];
	if(ESresult!=EBIES_Success) return ESresult;
	
	//*****************Set AppEUI / DevEUI*********************
	
	this->_payloadPtr[0] = (uint8_t)EBICMD_PhysicalAddress;
	for(int i=0;i<8;i++) this->_payloadPtr[1+i] = AppEUI[i];	
	for(int i=0;i<8;i++) this->_payloadPtr[9+i] = DevEUI[i];
	
	ESresult = this->performTxRx(17);
	if(ESresult!=EBIES_Success) return ESresult;
	ESresult = (ExecutionStatus)this->_rx_buffer[3];
	if(ESresult!=EBIES_Success) return ESresult;
	
	//********************AppKey*****************************
	this->_payloadPtr[0] = (uint8_t)EBICMD_NetworkSecurity;
	this->_payloadPtr[1] = 0x01;
	for(int i=0;i<16;i++) this->_payloadPtr[2+i] = AppKey[i];	

	ESresult = this->performTxRx(18);
	if(ESresult!=EBIES_Success) return ESresult;
	ESresult = (ExecutionStatus)this->_rx_buffer[3];
	if(ESresult!=EBIES_Success) return ESresult;
	
	//********************AppSKey*****************************
	this->_payloadPtr[0] = (uint8_t)EBICMD_NetworkSecurity;
	this->_payloadPtr[1] = 0x11;
	for(int i=0;i<16;i++) this->_payloadPtr[2+i] = AppSKey[i];	

	ESresult = this->performTxRx(18);
	if(ESresult!=EBIES_Success) return ESresult;
	ESresult = (ExecutionStatus)this->_rx_buffer[3];
	if(ESresult!=EBIES_Success) return ESresult;
	
	//********************NetSKey*****************************
	this->_payloadPtr[0] = (uint8_t)EBICMD_NetworkSecurity;
	this->_payloadPtr[1] = 0x10;
	for(int i=0;i<16;i++) this->_payloadPtr[2+i] = NetSKey[i];	

	ESresult = this->performTxRx(18);
	if(ESresult!=EBIES_Success) return ESresult;
	ESresult = (ExecutionStatus)this->_rx_buffer[3];
	if(ESresult!=EBIES_Success) return ESresult;
	
	//********************DevAddress*****************************
	this->_payloadPtr[0] = (uint8_t)EBICMD_NetworkAddress;
	for(int i=0;i<4;i++) this->_payloadPtr[1+i] = DevAddress[i];	
	ESresult = this->performTxRx(5);
	if(ESresult!=EBIES_Success) return ESresult;
	ESresult = (ExecutionStatus)this->_rx_buffer[3];
	if(ESresult!=EBIES_Success) return ESresult;

	
	//*********************Configure Class********************************
	this->_payloadPtr[0] = (uint8_t)EBICMD_EnergySave;
	this->_payloadPtr[1] = (uint8_t)lclass;	
	
	ESresult = this->performTxRx(2);
	if(ESresult!=EBIES_Success) return ESresult;
	ESresult = (ExecutionStatus)this->_rx_buffer[3];
	if(ESresult!=EBIES_Success) return ESresult;
	
	//*********************Actually Start the Network*********************
	this->_payloadPtr[0] = (uint8_t)EBICMD_NetworkStart;
	
	//START RECEIVING REPLY
	ESresult = this->performTxRx(1);
	if(ESresult!=EBIES_Success) return ESresult;
	ESresult = (ExecutionStatus)this->_rx_buffer[3];
	if(ESresult!=EBIES_Success) return ESresult;	
}

ExecutionStatus EBI_LoRaWAN::reset(){
	int replyLength;
	_DlDataAvailable = false;
	_disableDutyCycle = false;
	this->_payloadPtr[0] = (uint8_t)EBICMD_Reset;
	
	this->performTxRx(1);
	
	ExecutionStatus result = (ExecutionStatus)this->_rx_buffer[3];
	
	delay(100);
	
	//flush Serial rx buffer
	EBI_SERIAL.readBytes(_rx_buffer,EBI_SERIAL.available());
	
	//Wait until DeviceStatus is received
	ExecutionStatus ESresult = parseRxRaw((uint8_t)EBICMD_DeviceState,&replyLength);
	if(ESresult!=EBIES_Success) return ESresult;
	if(_rx_buffer[3]!=(uint8_t)EBIDS_ReadySuccess) return EBIES_GenericError;
	
	//*********************Read Current DevEUI*********************
	this->_payloadPtr[0] = (uint8_t)EBICMD_PhysicalAddress;
	
	//START RECEIVING REPLY	
	ESresult = this->performTxRx(1,&replyLength);
	if(this->performTxRx(1)!=EBIES_Success) return ESresult;
	
	//Final Parsing
	if(replyLength!=20) return EBIES_GenericError;
	for(int i=0;i<8;i++) AppEUI[i] = this->_rx_buffer[3+i];
	for(int i=0;i<8;i++) DevEUI[i] = this->_rx_buffer[11+i];
	
	return result;
}

/*******************************************/
/*			EBI Interface	 			   */
/*******************************************/
int EBI::getLastRxRSSI(){
	return _lastRxRSSI;
}

int EBI::getLastRxFPort(){
	return _lastRxFPort;
}

EBI::EBI(DeviceType type) {
	this->_type = type;
	pinMode(SW_RESET_PIN, OUTPUT);
	digitalWrite(SW_RESET_PIN, LOW);
}

int EBI::getLastTxRetransmissions(){
	return _lastTxRetries;
}
/*
int EBI::getLastTxDatarate(){
	switch(_lastTxDR){
		case DR6_SF7_BW250:
			return 6;
			break;
		case DR5_SF7_BW125:
			return 5;
			break;
		case DR4_SF8_BW125:
			return 4;
			break;
		case DR3_SF9_BW125:
			return 3;
			break;
		case DR2_SF10_BW125:
			return 2;
			break;
		case DR1_SF11_BW125:
			return 1;
			break;
		case DR0_SF12_BW125:
			return 0;
			break;
		default:
			return -1;
			break;
	}
}

int EBI::getLastTxSF(){
	switch(_lastTxDR){
		case DR6_SF7_BW250:
			return 7;
			break;
		case DR5_SF7_BW125:
			return 7;
			break;
		case DR4_SF8_BW125:
			return 8;
			break;
		case DR3_SF9_BW125:
			return 9;
			break;
		case DR2_SF10_BW125:
			return 10;
			break;
		case DR1_SF11_BW125:
			return 11;
			break;
		case DR0_SF12_BW125:
			return 12;
			break;
		default:
			return -1;
			break;
	}
}

int EBI::getLastTxBW(){
	switch(_lastTxDR){
		case DR6_SF7_BW250:
			return 250;
			break;
		case DR5_SF7_BW125:
			return 125;
			break;
		case DR4_SF8_BW125:
			return 125;
			break;
		case DR3_SF9_BW125:
			return 125;
			break;
		case DR2_SF10_BW125:
			return 125;
			break;
		case DR1_SF11_BW125:
			return 125;
			break;
		case DR0_SF12_BW125:
			return 125;
			break;
		default:
			return -1;
			break;
	}
}
*/
int EBI::getLastTxAckRSSI(){
	return _lastTxAckRSSI;
}

int EBI::getLastTxPower(){
	return _lastTxPtx;
}

int EBI::getLastTxWaitingTime(){
	return _lastTxWaitingTime;
}

void EBI::setRetransmissions(int retransmissions){
	if(retransmissions>255) this->_retransmissions = 0xFF;
	else if(retransmissions<0) this->_retransmissions = 0x01;
	else this->_retransmissions = (uint8_t)retransmissions;
}

ExecutionStatus EBI::disconnect(){
	int payloadLength = 1;
	this->_payloadPtr[0] = (uint8_t)EBICMD_NetworkStop;
	
	ExecutionStatus ESresult = this->performTxRx(1);
	if(ESresult!=EBIES_Success) return ESresult;
	
	return (ExecutionStatus)this->_rx_buffer[3];	
}

bool EBI::isOnline(){
	return this->deviceState()==EBIDS_Online;
}

DeviceStatus EBI::deviceState(){
	this->_payloadPtr[0] = (uint8_t)EBICMD_DeviceState;
	
	this->performTxRx(1);
	
	return (DeviceStatus)this->_rx_buffer[3];
}

/*******************************************/
/*			BASIC FUNCTIONS 			   */
/*******************************************/

void EBI::computeLengthChecksum(int payloadLength){
	//Length field
	uint16_t lengthField = payloadLength+3;
	this->_tx_buffer[0] = (lengthField>>8) & 0xFF;
	this->_tx_buffer[1] = (lengthField>>0) & 0xFF;
	
	uint8_t checksum = 0x00;
	//CRC Field
	for(int i=0;i<payloadLength+2;i++) checksum += this->_tx_buffer[i];
	this->_tx_buffer[payloadLength+2] = checksum;
}

ExecutionStatus EBI::parseRxRaw(uint8_t expectedCommand, int *replyLength){	
	ExecutionStatus result = this->parseRxRaw(replyLength);
	if(result!=EBIES_Success){
		//Clean RX Buffer
		replyLength = 0;
		return result;
	} 
	//check if right command
	if(this->_rx_buffer[2] != (0x80|expectedCommand)){
		DEBUG_PRINTLN("Wrong Command");
		return EBIES_GenericError;
	}
	return EBIES_Success;
}

ExecutionStatus EBI::parseRxRaw(int *replyLength){
	this->_RxTimerStart = millis();
	if(EBI_SERIAL.readBytes(this->_rx_buffer,2)!=2) {
		DEBUG_PRINT("TIMEOUT!");
		return EBIES_OperationTimeout;
	}
	
	//parse length
	*replyLength = (this->_rx_buffer[0] << 8) + this->_rx_buffer[1];
	
	EBI_SERIAL.readBytes(&this->_rx_buffer[2],*replyLength-2);
	
	
	//checksum check
	uint8_t checksum = 0x00;
	for(int i=0;i<*replyLength-1;i++) checksum += this->_rx_buffer[i];	
	if(checksum!=this->_rx_buffer[*replyLength-1]){
		DEBUG_PRINTLN("Wrong Checksum");
		return EBIES_GenericError;
	}
	return EBIES_Success;
}

ExecutionStatus EBI::performTxRx(int payloadLength){
	int replyLength;
	return this->performTxRx(payloadLength, &payloadLength);
}

ExecutionStatus EBI::performTxRx(int payloadLength, int *replyLengthPtr){
	this->computeLengthChecksum(payloadLength);
	
	EBI_SERIAL.write(this->_tx_buffer, payloadLength+3);
	
	DEBUG_PRINT("---> ");
	DEBUG_BUFFER_PRINTLN((uint8_t *)this->_tx_buffer,payloadLength+3);
	
	//START RECEIVING REPLY
	ExecutionStatus res = this->parseRxRaw(this->_payloadPtr[0],replyLengthPtr);	
	if(res==EBIES_Success){
		DEBUG_PRINT("\t<--- ");
		DEBUG_BUFFER_PRINTLN((uint8_t *)this->_rx_buffer,*replyLengthPtr);
	}
	return res;
}

#ifdef EBI_DEBUG
  static void DEBUG_BUFFER_PRINTLN(uint8_t *buffer, int length){
	  for(int i=0;i<length-1;i++) {
		  if(buffer[i]<16) DEBUG_PRINT("0")
		  DEBUG_PRINT(buffer[i], HEX)
		  DEBUG_PRINT(" ")
	  }
	  if(buffer[length-1]<16) DEBUG_PRINT("0")
	  DEBUG_PRINTLN(buffer[length-1], HEX)		  
  }
#else
  static void DEBUG_BUFFER_PRINTLN(uint8_t *buffer, int length){}
#endif
