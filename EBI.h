/**< EBI Interface

written by Luca Feltrin
*/
#ifndef EBI_H
#define EBI_H

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

#define	BUF_SIZE	256
#define SW_RESET_PIN	8
#define DL_TIMEOUT_MS	1000

// Uncomment to enable printing out nice debug messages.
//#define EBI_DEBUG


#ifdef ARDUINO_AVR_UNO
	#define DEBUG_PRINTER Serial
	#define EBI_SERIAL ebiSerial
	#define ENABLE_SW_SERIAL
#elif ARDUINO_SAMD_ZERO
	#define DEBUG_PRINTER Serial
	#define EBI_SERIAL	Serial1
#elif ARDUINO_SAMD_MKRZERO
	#define DEBUG_PRINTER Serial
	#define EBI_SERIAL	Serial1
#else
	#define DEBUG_PRINTER Serial
	#define EBI_SERIAL ebiSerial
	#define ENABLE_SW_SERIAL
#endif


#ifdef ENABLE_SW_SERIAL
	#include <SoftwareSerial.h>
	static SoftwareSerial ebiSerial(2, 3);	
#endif


// Define where debug output will be printed.

// Setup debug printing macros.
static void DEBUG_BUFFER_PRINTLN(uint8_t *buffer, int length);
#ifdef EBI_DEBUG
	#define RX_TIMEOUT_MS	45000
	#define DEBUG_PRINT(...) { DEBUG_PRINTER.print(__VA_ARGS__); }
	#define DEBUG_PRINTLN(...) { DEBUG_PRINTER.println(__VA_ARGS__); }  
#else
	#define RX_TIMEOUT_MS	45000
	#define DEBUG_PRINT(...) {}
	#define DEBUG_PRINTLN(...) {}
#endif
#define TIMEOUT_EPSILON_MS	50

/**
*	Datarate Setting.
*	Datarate Setting defined as bit mask.
*/
enum DatarateMask{
	DR6_SF7_BW250 = 0x40,
	DR5_SF7_BW125 = 0x20,
	DR4_SF8_BW125 = 0x10,
	DR3_SF9_BW125 = 0x08,
	DR2_SF10_BW125 = 0x04,
	DR1_SF11_BW125 = 0x02,
	DR0_SF12_BW125 = 0x01,
};	

/**
*	Device Types
*	Technology used by the transceiver, defines the set of commands available and their usage.
*/
enum DeviceType{
	WMBUS,			/**< Wireless MBUS (not implemented) */
	IEEE802154,		/**< IEEE 802.15.4 (not implemented) */
	Zigbee,			/**< Zigbee (not implemented) */
	LoRaWAN,		/**< LoRaWAN protocol */
	LoRaEMB			/**< LoRa + Embit protocol (not implemented) */
};	

/**
*	LoRaWAN Class
*	LoRaWAN Class definition
*/
enum LoRaClass{
	ClassA=0x01,		/**< Two Reception Windows */
	ClassC=0x00,		/**< Always Listening */
};

/**
*	LoRaWAN Join Mode
*	Type of Join according to LoRaWAN standard
*/
enum LoRaJoinMode{
	ABP=0x00,				/**< Activation by Personalization (AppSKey, NetSKey, DevAddress must be configured) */
	OTAA=0x01,				/**< Over the Air Activation (AppSKey, NetSKey, DevAddress are assigned by the network) */
};	

/**
*	Message ID
*	Code identifying each possible command (third byte of the packet)
*/
enum MessageId{
	EBICMD_DeviceInformation = 0x01,
	EBICMD_DeviceState = 0x04,
	EBICMD_Reset = 0x05,
	EBICMD_FirmwareVersion = 0x06,
	EBICMD_RestoreToFactoryDefaultSettings = 0x07,
	EBICMD_SaveSettings = 0x08,
	EBICMD_SerialPortConfiguration = 0x09,
	EBICMD_OutputPower = 0x10,
	EBICMD_OperatingChannel = 0x11,
	EBICMD_ActiveChannelMask = 0x12,
	EBICMD_EnergySave = 0x13,
	EBICMD_ForceSleep = 0x14,
	EBICMD_ForceDataPoll = 0x15,
	EBICMD_TimerControl = 0x1D,
	EBICMD_ADCControl = 0x1E,
	EBICMD_GPIOControl = 0x1F,
	EBICMD_PhysicalAddress = 0x20,
	EBICMD_NetworkAddress = 0x21,
	EBICMD_NetworkIdentifiers = 0x22,
	EBICMD_NetworkRole = 0x23,
	EBICMD_NetworkAutomatedSettings = 0x24,
	EBICMD_NetworkPreferences = 0x25,
	EBICMD_NetworkSecurity = 0x26,
	EBICMD_NetworkType = 0x2D,
	EBICMD_MacCounters = 0x2E,
	EBICMD_NetworkStop = 0x30,
	EBICMD_NetworkStart = 0x31,
	EBICMD_NetworkScan = 0x32,
	EBICMD_AddEndpoint = 0x38,
	EBICMD_RemoveEndpoint = 0x39,
	EBICMD_AssociatedAddresses = 0x40,
	EBICMD_AssociatingDevice = 0x41,
	EBICMD_SendData = 0x50,
	EBICMD_ReceivedData = 0x60,
	EBICMD_EnterBootloader = 0x70,
	EBICMD_SetBootloaderOptions = 0x71,
	EBICMD_EraseMemory = 0x78,
	EBICMD_WriteMemoryChunk = 0x7A,
	EBICMD_ReadMemoryChunk = 0x7B,
	EBICMD_CommitFirmware = 0x7F,
};

/**
* Network Type.
* LoRaWAN network type which has an impact on the sync word.
*/
enum EBI_NetworkType{
	EBINT_Public=0x00,		/**< Public Network, configure the sync word to 0x34. */
	EBINT_Private=0x01,		/**< Private Network, configure the sync word to 0x12. */
};

/**
* Execution Status.
* interpreted as an acknowledge return value.
*/
enum ExecutionStatus{
	EBIES_Success = 0x00,					/**< Success. */
	EBIES_GenericError = 0x01,				/**< Generic Error. */
	EBIES_ParametersNotAccepted = 0x02,		/**< One of the input parameter is not valid or is missing. */
	EBIES_OperationTimeout = 0x03,			/**< A Timeout has expired. */
	EBIES_NoMemory = 0x04,					/**< No Memory. */
	EBIES_Unsupported = 0x05,				/**< Unsupported command. */
	EBIES_Busy = 0x06,						/**< the device is busy. */
	EBIES_Unknown,							/**< Unknown Error. */
};

/**
*	Device Status
*	Status of the transceiver \sa deviceState
*/
enum DeviceStatus{
	EBIDS_Booting = 0x00,
	EBIDS_InsideBootloader = 0x01,
	EBIDS_ReadySuccess = 0x10,
	EBIDS_ReadyFail = 0x11,
	EBIDS_Offline = 0x20,
	EBIDS_Connecting = 0x21,
	EBIDS_TransparentModeStartup = 0x22,
	EBIDS_Online = 0x30,
	EBIDS_Disconnecting = 0x40,
	EBIDS_Reserved = 0x50,
	EBIDS_EndOfRxWindow = 0x51,
};


/**
*	EBI Class
*	Generic transceiver handler
*/
class EBI {
	public:
		EBI(DeviceType type);				/**< Creates a handler of the transceiver \param type technology used */
		virtual ExecutionStatus begin()=0;			/**< Initializes the EBI interface towards the transceiver */
		virtual ExecutionStatus reset()=0;			/**< Resets the transceiver */
		DeviceStatus deviceState();			/**< Gets the transceiver state */
		bool isOnline();					/**< Returns true if device state is Online */
		virtual ExecutionStatus connect() = 0;								/**< Connects to the Network with the current settings (may result in a Timeout) */
		ExecutionStatus disconnect();							/**< Disconnect from the network */
		void setRetransmissions(int retransmissions); /**< set the number of transmission attempts in case of ACK packets are used [1..255] */
		int getLastTxRetransmissions();		/**< returns the number of transmission attempts performed during the last transmission */
		//int getLastTxDatarate();			/**< returns the Datarate used in the last transmission */
		//int getLastTxSF();					/**< returns the Spreading Factor used in the last transmission */
		//int getLastTxBW();					/**< returns the Bandwidth used in the last transmission */
		int getLastTxAckRSSI();				/**< returns the RSSI of the Ack received during the last transmission */
		int getLastTxPower();				/**< returns the Transmit Power used in the last transmission */
		int getLastTxWaitingTime();			/**< returns the Time elapsed during the last transmission (in ms)*/
		int getLastRxRSSI();				/**< returns the RSSI of the last received transmission */
		int getLastRxFPort();				/**< returns the FPort of the last received transmission */
		int transmitPower;		/**< Transmit Power in dBm, must be set after begin(), modification will take place at next connect(). */
	protected:
		int _lastTxRetries=-1;
		int _lastTxAckRSSI=0;
		int _lastRxRSSI=0;
		int _lastRxFPort=0;
		uint8_t _lastTxDR;
		uint16_t _lastTxChannel;
		int _lastTxPtx;
		int _lastTxWaitingTime;
		DeviceType _type;
		uint8_t _retransmissions = 3;
		uint8_t _tx_buffer[BUF_SIZE];
		uint8_t _rx_buffer[BUF_SIZE];
		uint8_t _dl_buffer[BUF_SIZE];
		uint8_t *_payloadPtr;
		void computeLengthChecksum(int payloadLength);	/**< Compute the Length and Checksum field \param payloadLength payload length in bytes (payload + command code) */		
		ExecutionStatus parseRxRaw(uint8_t expectedCommand, int *replyLength);		/**< Waits for a packet, checks the checksum and fill the rx buffer \param expectedCommand expected command code \param replyLength pointer to reply length (output) */
		ExecutionStatus parseRxRaw(int *replyLength);		/**< Waits for a packet, checks the checksum and fill the rx buffer \param replyLength pointer to reply length (output) */		
		ExecutionStatus performTxRx(int payloadLength, int *replyLengthPtr);						/**< Performs the transmission of the TX_Buffer and wait for the reception on the serial until the RX Buffer is ready to be read \param payloadLength number of payload bytes to send (no CRC and length) \param replyLengthPtr pointer to reply length variable*/
		ExecutionStatus performTxRx(int payloadLength);						/**< Performs the transmission of the TX_Buffer and wait for the reception on the serial until the RX Buffer is ready to be read \param payloadLength number of payload bytes to send (no CRC and length)*/
		long int _RxTimerStart;		
};

/**
*	EBI LoRaWAN Class
*	Handler for LoRaWAN Embit module
*/
class EBI_LoRaWAN: public EBI{
	public:
		EBI_LoRaWAN(): EBI(LoRaWAN) {};						/**< Creates a handler of the transceiver \param type technology used */
		ExecutionStatus begin();							/**< Initializes the EBI interface towards the transceiver */
		ExecutionStatus enableADR();							/**< Enables Adaptive Data Rate Feature (LoRaWAN only) */
		ExecutionStatus disableADR();							/**< Enables Adaptive Data Rate Feature (LoRaWAN only) */
		ExecutionStatus sendBytes(uint8_t data[], int length, int FPort);		/**< Send an array of bytes \param data array of bytes to send \param length number of bytes to send \param FPort, allowed values [1..223]*/
		ExecutionStatus sendInt(int value, int FPort);				/**< Send an int \param value data to send \param FPort LoRaWAN FPort, allowed values [1..223]*/
		ExecutionStatus sendFloat(float value, int FPort);			/**< Send a float \param value data to send \param FPort LoRaWAN FPort, allowed values [1..223]*/
		ExecutionStatus disableDutyCycle();			/**< disables the Duty Cycle limit */
		void enableAck();		/**< enable the use of ACK packets for each transmission */
		void disableAck();		/**< disable the use of ACK packets for each transmission */
		ExecutionStatus connect();								/**< Connects to the Network with the current settings (may result in a Timeout) */
		ExecutionStatus reset();			/**< Resets the transceiver */		
		int available();			/**< Get the number of bytes available for reading from the LoRa downlink. Returns immediately if no bytes are available*/
		int available(int timeout);	/**< Get the number of bytes available for reading from the LoRa downlink. If no data is available waits a given amount of time waiting to receive something (only Class C) \param timeout number of ms to wait before returning if no bytes are available*/
		int readBytes(uint8_t buffer[], int length);	/**< reads the received data and transfer it in a given buffer. Returns the number of char actually read \param length number of bytes to read \param buffer buffer to fill with data*/
		int getUplinkMacCounter();		/**< Get the current Uplink MAC Counter. */
		int getDownlinkMacCounter();	/**< Get the current Downlink MAC Counter. */
		EBI_NetworkType networkType;	/**< current network type, set after begin(), modification will take place at next connect(). */
		LoRaClass lclass;	/**< current LoRaWAN Class, set after begin(), modification will take place at next connect(). */
		uint8_t DevEUI[8];	/**< the current DevEUI configured in the module, set after begin(), modification will take place at next connect()*/
		uint8_t AppEUI[8];	/**< the current AppEUI configured in the module, set after begin(), modification will take place at next connect()*/
		uint8_t AppKey[16];	/**< App Key, must be set after begin(), modification will take place at next connect(), it is not possible to read the initial value. */
		uint8_t AppSKey[16];	/**< AppSKey, must be set after begin(), modification will take place at next connect(), it is not possible to read the initial value. */
		uint8_t NetSKey[16];	/**< NetSKey, must be set after begin(), modification will take place at next connect(), it is not possible to read the initial value. */
		uint8_t DevAddress[4]; /**< DevAddress, must be set after begin() if ABP join mode is used, modification will take place at next connect(), if OTAA mode is used it is not readable */
		LoRaJoinMode joinMode;	 /**< Join mode used by the device, must be set after begin(), modification will take place at next connect(). */
	private:
		int _DlDataAvailable=-1;
		bool _ACK = false;
		bool _ADR = false;
		bool _disableDutyCycle = false;
};

#endif
