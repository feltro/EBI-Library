# EBI-Library
This is an Arduino library for Embit Modules with EBI Interface.

This library was created for the Long Range IoT Hackathon 2018 in Bologna. More information and documentation [here](http://longrangeiothackathon.org).