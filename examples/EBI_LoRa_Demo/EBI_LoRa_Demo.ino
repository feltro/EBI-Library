// Example testing sketch for EMB-LR1272E (Embit srl, Modena, Italy)
// Written by Luca Feltrin, University of Bologna, public domain

#include "EBI.h"
uint8_t rxBuffer[64];

uint8_t AppSKey[] = {0x93, 0xB0, 0x59, 0x24, 0x1F, 0x8A, 0xB5, 0x82, 0xC3, 0x08, 0x01, 0x42, 0x27, 0xB2, 0x60, 0x28};
uint8_t NetSKey[] = {0x4F, 0x0F, 0xB3, 0xF1, 0xE8, 0x91, 0x79, 0x10, 0x6C, 0xE7, 0xFF, 0x8C, 0x7D, 0x80, 0x6A, 0xD4};

EBI_LoRaWAN hEbi;

void setup() {
  Serial.begin(115200);     //Debug
  Serial.print("Starting...");
  if(hEbi.begin()!=EBIES_Success) Serial.println("Failed");
  Serial.println("Done!");

  hEbi.enableADR();
  hEbi.joinMode = ABP;
  hEbi.disableDutyCycle();
  for(int i=0;i<16;i++) hEbi.AppSKey[i] = AppSKey[i];
  for(int i=0;i<16;i++) hEbi.NetSKey[i] = NetSKey[i];
  for(int i=0;i<4;i++) hEbi.DevAddress[i] = hEbi.DevEUI[i+12];  //DevAddress is the last 4 bytes of DevEUI
  hEbi.enableAck();
  hEbi.lclass = ClassA;

  Serial.print("Connecting...");
  while(hEbi.connect()!=EBIES_Success){
    Serial.print("Failed...\n\tNew Attempt...");
  }  
  Serial.println("Done!");
}

void loop() {
  delay(10000);
  
  Serial.print("Sending new data...");
  if(EBIES_Success == hEbi.sendInt(analogRead(A0),6)) {   //Send Analog Value using FPort=6
    Serial.print("Success! (ACK RSSI = ");
    Serial.print(hEbi.getLastTxAckRSSI());
    Serial.println(" dBm)");
    
    int dl_length = hEbi.available(); //Check if downlink data has been received during last transmission
    if(dl_length>0){
      int datalength = hEbi.readBytes(rxBuffer,dl_length);
      Serial.print("Received Data: ");
      for(int i=0;i<datalength;i++){
        if(rxBuffer[i]<16) Serial.print("0");
        Serial.print(rxBuffer[i],HEX);
        Serial.print(" ");
      }
      Serial.print(" (RSSI = ");
      Serial.print(hEbi.getLastRxRSSI());
      Serial.println(" dBm)");
    }
  } else {
    Serial.println("Failed");
  }
}