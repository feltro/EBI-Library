/* Example testing sketch for EMB-LR1272E (Embit srl, Modena, Italy)
 * Written by Luca Feltrin, University of Bologna, public domain
 * 
 * Example for ClassA LoRaWAN devices, ABP Join with downlink
 * Transmit the analog value from A0
 */

#include "EBI.h"

uint8_t AppSKey[] = {0x3C, 0x94, 0x49, 0xCB, 0x5B, 0xED, 0xEF, 0x98, 0x46, 0xEB, 0x21, 0xE5, 0x81, 0x30, 0x74, 0x1C};
uint8_t NetSKey[] = {0x19, 0x36, 0x8A, 0x25, 0x55, 0x91, 0xD8, 0xB5, 0xBD, 0xFD, 0x1C, 0xD3, 0x18, 0x81, 0x22, 0x35};
uint8_t DevAddress[] = {0x70, 0x10, 0x3d, 0x7f};

uint8_t rxBuffer[64];

EBI_LoRaWAN hEbi;

void setup() {
  Serial.begin(115200);           //Debug Serial
  Serial.print("Starting...");
  while(hEbi.begin()!=EBIES_Success) Serial.println("Failed");
  Serial.println("Done!");

  for(int i=0;i<16;i++) hEbi.AppSKey[i] = AppSKey[i];
  for(int i=0;i<16;i++) hEbi.NetSKey[i] = NetSKey[i];
  for(int i=0;i<4;i++) hEbi.DevAddress[i] = DevAddress[i];
  hEbi.lclass = ClassA;
  hEbi.joinMode = ABP;
  hEbi.transmitPower = 14;    //Set maximum power
  hEbi.enableAck();

  Serial.print("Connecting...");
  while(hEbi.connect()!=EBIES_Success){
    Serial.print("Failed...\n\tNew Attempt...");
  }  
  Serial.println("Done!");
}

void loop() {
  delay(10000);
  
  Serial.print("Sending new data...");
  if(EBIES_Success == hEbi.sendInt(analogRead(A0),6)) {   //Send Analog Value using FPort=6
    Serial.print("Success! (ACK RSSI = ");
    Serial.print(hEbi.getLastTxAckRSSI());
    Serial.println(" dBm)");
    
    int dl_length = hEbi.available(); //Check if downlink data has been received during last transmission
    if(dl_length>0){
      int datalength = hEbi.readBytes(rxBuffer,dl_length);
      Serial.print("Received Data: ");
      for(int i=0;i<datalength;i++){
        if(rxBuffer[i]<16) Serial.print("0");
        Serial.print(rxBuffer[i],HEX);
        Serial.print(" ");
      }
      Serial.print(" (RSSI = ");
      Serial.print(hEbi.getLastRxRSSI());
      Serial.println(" dBm)");
    }
  } else {
    Serial.println("Failed");
  }
  
}